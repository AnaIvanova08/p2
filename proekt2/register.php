<?php
include 'config.php';

if (isset($_POST['submit'])) {

    $name = $_POST['name'];
    $name = filter_var($name, FILTER_SANITIZE_STRING);
    $email = $_POST['email'];
    $email = filter_var($email, FILTER_SANITIZE_STRING);
    $pass = md5($_POST['pass']);
    $pass = filter_var($pass, FILTER_SANITIZE_STRING);
    $cpass = md5($_POST['cpass']);
    $cpass = filter_var($cpass, FILTER_SANITIZE_STRING);



    $select = $conn->prepare("SELECT * FROM `users` WHERE email = ?");
    $select->execute([$email]);

    if ($select->rowCount() > 0) {
        $message[] = 'user already exist!';
    } else {
        if ($pass != $cpass) {
            $message[] = 'confirm password not matched!';
        } else {
            $insert = $conn->prepare("INSERT INTO `users`(name, email, password) VALUES(?,?,?)");
            $insert->execute([$name, $email, $cpass]);
            if ($insert) {

                $message[] = 'registered succesfully!';
                header('location:login.php');
            }
        }
    }
}
?>


<html lang="en" class="scroll-smooth">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">



    <title>Brainster Library</title>
    <link rel="stylesheet" href="./css/output.css">
    <link rel="stylesheet" href="style.css">


    <script defer="" src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>
</head>

<body>

    <?php
    if (isset($message)) {
        foreach ($message as $message) {
            echo '
         <div class="message">
            <span>' . $message . '</span>
            <i class="fas fa-times" onclick="this.parentElement.remove();"></i>
         </div>
         ';
        }
    }
    ?>

    <section class="form-container">

        <form action="" method="post" enctype="multipart/form-data">
            <h3>register now</h3>
            <input type="text" required placeholder="enter your username" class="box" name="name">
            <input type="email" required placeholder="enter your email" class="box" name="email">
            <input type="password" required placeholder="enter your password" class="box" name="pass">
            <input type="password" required placeholder="confirm your password" class="box" name="cpass">

            <p>already have an account? <a href="login.php">login now</a></p>
            <input type="submit" value="register now" class="btn" name="submit">
        </form>

    </section>


    </div>
</body>

</html>